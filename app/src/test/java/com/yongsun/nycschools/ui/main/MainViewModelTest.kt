package com.yongsun.nycschools.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.yongsun.nycschools.data.model.SchoolFaker
import com.yongsun.nycschools.data.repository.SchoolRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import kotlinx.coroutines.flow.flow
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule

class MainViewModelTest {

    companion object {
        private val SCHOOL = SchoolFaker.basic()
        private val FLOW = flow { emit(PagingData.from(listOf(SchoolFaker.basic()))) }
        private val CACHED_FLOW = flow { emit(PagingData.from(listOf(SchoolFaker.basic()))) }
    }

    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    private val repository: SchoolRepository = mockk()
    private lateinit var viewModel: MainViewModel

    @Before
    fun setup() {
        viewModel = MainViewModel(repository)
        mockkStatic("androidx.paging.CachedPagingDataKt") // for extension function
        every { repository.fetchSchools() } returns FLOW
        every { FLOW.cachedIn(viewModel.viewModelScope) } returns CACHED_FLOW
    }

    @Test
    fun `test fetchSchools returns flow from repository`() {
        val flow = viewModel.fetchSchools()

        assertEquals(CACHED_FLOW, flow)
    }

    @Test
    fun `test onItemClick assigns school to schoolClickEvent`() {
        viewModel.onItemClick(SCHOOL)

        assertEquals(SCHOOL, viewModel.schoolClickEvent.value)
    }
}