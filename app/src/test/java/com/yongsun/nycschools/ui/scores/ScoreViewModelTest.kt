package com.yongsun.nycschools.ui.scores

import android.view.View
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.yongsun.nycschools.data.model.GenericFaker
import com.yongsun.nycschools.data.model.Score
import com.yongsun.nycschools.data.model.ScoreFaker
import com.yongsun.nycschools.data.repository.SchoolRepository
import com.yongsun.nycschools.ui.TestCoroutineRule
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import retrofit2.Response

@ExperimentalCoroutinesApi
class ScoreViewModelTest {

    companion object {
        private val SCORE = ScoreFaker.basic()
        private val DBN = GenericFaker.string()
    }

    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val repository: SchoolRepository = mockk()

    private val response: Response<List<Score>> = mockk()

    private lateinit var viewModel: ScoreViewModel

    @Before
    fun setup() {
        viewModel = ScoreViewModel(repository)
        coEvery { repository.fetchScore(DBN) } returns response
        viewModel.spinnerVisibility.value = View.VISIBLE
    }

    @Test
    fun `test fetchScore does not trigger network call when score is not null`() {
        viewModel.score.value = SCORE

        viewModel.fetchScore(DBN)

        verify(exactly = 0) {
            testCoroutineRule.runBlockingTest {
                repository.fetchScore(DBN)
            }
        }
    }

    @Test
    fun `test fetchScore triggers network call when score is null`() {
        viewModel.score.value = null

        viewModel.fetchScore(DBN)

        verify {
            testCoroutineRule.runBlockingTest {
                repository.fetchScore(DBN)
            }
        }
    }

    @Test
    fun `test fetchScore happy path`() {
        viewModel.score.value = null
        every { response.isSuccessful } returns true
        every { response.body() } returns listOf(SCORE)

        viewModel.fetchScore(DBN)

        assertEquals(SCORE, viewModel.score.value)
        assertEquals(View.GONE, viewModel.errorVisibility.value)
        assertEquals(View.GONE, viewModel.spinnerVisibility.value)
    }

    @Test
    fun `test fetchScore unhappy path`() {
        viewModel.score.value = null
        every { response.isSuccessful } returns false

        viewModel.fetchScore(DBN)

        assertEquals(null, viewModel.score.value)
        assertEquals(View.VISIBLE, viewModel.errorVisibility.value)
        assertEquals(View.GONE, viewModel.spinnerVisibility.value)
    }
}
