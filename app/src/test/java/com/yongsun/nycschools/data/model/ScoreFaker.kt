package com.yongsun.nycschools.data.model

object ScoreFaker {
    fun basic(
        dbn: String = GenericFaker.string(),
        schoolName: String = GenericFaker.string(),
        criticalReadingScore: String = GenericFaker.string(),
        mathScore: String = GenericFaker.string(),
        readingScore: String = GenericFaker.string()
    ) = Score(
        dbn, schoolName, criticalReadingScore, mathScore, readingScore
    )
}