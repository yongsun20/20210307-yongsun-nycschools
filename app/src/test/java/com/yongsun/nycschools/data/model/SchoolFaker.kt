package com.yongsun.nycschools.data.model

object SchoolFaker {
    fun basic(
        dbn: String = GenericFaker.string(),
        school_name: String = GenericFaker.string(),
        overview_paragraph: String = GenericFaker.string(),
        phone_number: String = GenericFaker.string(),
        website: String = GenericFaker.string(),
        borough: String = GenericFaker.string()
    ) = School(
        dbn, school_name, overview_paragraph, phone_number, website, borough
    )
}