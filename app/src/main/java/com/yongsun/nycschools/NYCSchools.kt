package com.yongsun.nycschools

import android.app.Application
import com.yongsun.nycschools.di.appModule
import com.yongsun.nycschools.di.networkingModule
import com.yongsun.nycschools.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Application
 */
class NYCSchools: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@NYCSchools)
            modules(listOf(appModule(), networkingModule(), viewModelModule()))
        }
    }
}