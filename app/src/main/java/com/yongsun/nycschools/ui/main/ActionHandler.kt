package com.yongsun.nycschools.ui.main

import com.yongsun.nycschools.data.model.School

/**
 * Action Handler for databinding
 */
interface ActionHandler {
    fun onItemClick(school: School)
}