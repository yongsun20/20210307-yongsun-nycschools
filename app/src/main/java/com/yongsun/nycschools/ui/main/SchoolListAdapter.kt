package com.yongsun.nycschools.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yongsun.nycschools.data.model.School
import com.yongsun.nycschools.databinding.SchoolSummaryBinding

/**
 * List Adapter for displaying a list of articles
 */
class SchoolListAdapter(private val handler: ActionHandler) :
    PagingDataAdapter<School, SchoolListAdapter.SchoolSummaryViewHolder>(DiffUtilCallBack()) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SchoolSummaryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SchoolSummaryBinding.inflate(inflater, parent, false)
        return SchoolSummaryViewHolder(binding, handler)
    }

    override fun onBindViewHolder(holder: SchoolSummaryViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    class SchoolSummaryViewHolder(
        private val binding: SchoolSummaryBinding,
        private val handler: ActionHandler
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: School) {
            binding.school = item
            binding.handler = handler
            binding.executePendingBindings()
        }
    }
}

