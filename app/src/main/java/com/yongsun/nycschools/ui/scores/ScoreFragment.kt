package com.yongsun.nycschools.ui.scores

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.yongsun.nycschools.databinding.ScoreFragmentBinding
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Fragment that displays score details
 */
class ScoreFragment : Fragment() {

    companion object {
        fun newInstance(dbn: String): ScoreFragment {
            return ScoreFragment().apply {
                arguments = Bundle().apply {
                    putString("dbn", dbn)
                }
            }
        }
    }

    private val viewModel: ScoreViewModel by viewModel()
    private var binding: ScoreFragmentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ScoreFragmentBinding.inflate(inflater, container, false)
        binding?.lifecycleOwner = viewLifecycleOwner
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val dbn = arguments?.getString("dbn")
        binding?.viewModel = viewModel.apply {
            if (dbn != null) {
                fetchScore(dbn)
            }
        }
    }
}