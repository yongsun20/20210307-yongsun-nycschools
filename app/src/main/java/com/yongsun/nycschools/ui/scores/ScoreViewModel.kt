package com.yongsun.nycschools.ui.scores

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yongsun.nycschools.data.model.Score
import com.yongsun.nycschools.data.repository.SchoolRepository
import kotlinx.coroutines.launch

/**
 * ViewModel for [ScoreFragment]
 */
class ScoreViewModel(private val repository: SchoolRepository) : ViewModel() {

    val score = MutableLiveData<Score>()
    val errorVisibility = MutableLiveData(View.GONE)
    val spinnerVisibility = MutableLiveData(View.VISIBLE)

    /**
     * Fetch average SAT scores
     * @param dbn school ID
     */
    fun fetchScore(dbn: String) {
        if (score.value == null) {
            viewModelScope.launch {
                val response = repository.fetchScore(dbn)
                if (response.isSuccessful && response.body()?.isNotEmpty() == true) {
                    score.postValue(response.body()?.first())
                } else {
                    errorVisibility.postValue(View.VISIBLE)
                }
                spinnerVisibility.postValue(View.GONE)
            }
        }
    }
}