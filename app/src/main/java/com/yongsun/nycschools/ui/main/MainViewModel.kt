package com.yongsun.nycschools.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.hadilq.liveevent.LiveEvent
import com.yongsun.nycschools.data.model.School
import com.yongsun.nycschools.data.repository.SchoolRepository
import kotlinx.coroutines.flow.Flow

/**
 * ViewModel for [MainFragment]
 */
class MainViewModel(private val repository: SchoolRepository) : ViewModel(), ActionHandler {

    val schoolClickEvent = LiveEvent<School>()
    private var fetchedSchools: Flow<PagingData<School>>? = null

    fun fetchSchools(): Flow<PagingData<School>> {
        if (fetchedSchools != null) {
            return fetchedSchools!!
        }

        fetchedSchools = repository.fetchSchools().cachedIn(viewModelScope)
        return fetchedSchools!!
    }

    override fun onItemClick(school: School) {
        schoolClickEvent.value = school
    }
}