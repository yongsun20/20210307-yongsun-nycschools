package com.yongsun.nycschools.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.yongsun.nycschools.R
import com.yongsun.nycschools.data.model.School
import com.yongsun.nycschools.databinding.MainFragmentBinding
import com.yongsun.nycschools.ui.scores.ScoreFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Fragment for main page, which displays a list of Articles
 */
class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModel()
    private var binding: MainFragmentBinding? = null
    private lateinit var adapter: SchoolListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        setupObservers()
    }

    private fun setupUI() {
        this.adapter = SchoolListAdapter(viewModel)
        binding?.recyclerView?.let {
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = this.adapter.withLoadStateHeaderAndFooter(
                header = SchoolLoadingAdapter { adapter.retry() },
                footer = SchoolLoadingAdapter { adapter.retry() }
            )
            it.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun setupObservers() {
        lifecycleScope.launch {
            viewModel.fetchSchools().collectLatest {
                adapter.submitData(it)
            }
        }

        viewModel.schoolClickEvent.observe(viewLifecycleOwner, {
            navigateTo(it)
        })
    }

    private fun navigateTo(school: School) {
        parentFragmentManager
            .beginTransaction()
            .replace(R.id.container, ScoreFragment.newInstance(school.dbn))
            .addToBackStack(null).commit()
    }
}