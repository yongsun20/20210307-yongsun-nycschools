package com.yongsun.nycschools.ui.main

import androidx.recyclerview.widget.DiffUtil
import com.yongsun.nycschools.data.model.School

/**
 * ItemCallback for School
 */
class DiffUtilCallBack : DiffUtil.ItemCallback<School>() {
    override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem.dbn == newItem.dbn
    }

    override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem.dbn == newItem.dbn
    }
}