package com.yongsun.nycschools.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yongsun.nycschools.data.model.LoadingModel
import com.yongsun.nycschools.databinding.ItemLoadingBinding

/**
 * Adapter for header and footer loading view
 */
class SchoolLoadingAdapter(private val retry: () -> Unit) :
        LoadStateAdapter<SchoolLoadingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemLoadingBinding.inflate(inflater, parent, false)
        return ViewHolder(binding, retry)
    }

    override fun onBindViewHolder(holder: ViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    class ViewHolder(private val binding: ItemLoadingBinding, private val retry: () -> Unit) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(loadState: LoadState) {
            binding.model = LoadingModel(
                progressBarVisibility = if (loadState is LoadState.Loading) {
                    View.VISIBLE
                } else {
                    View.GONE
                },
                errorMessageVisibility = if (loadState !is LoadState.Loading) {
                    View.VISIBLE
                } else {
                    View.GONE
                },
                retryVisibility = if (loadState !is LoadState.Loading) {
                    View.VISIBLE
                } else {
                    View.GONE
                },
                errorMessage = if (loadState is LoadState.Error) {
                    loadState.error.localizedMessage
                } else {
                    "Error!"
                },
            )
            binding.handler = Handler(retry)
            binding.executePendingBindings()
        }
    }

    class Handler (private val retry: () -> Unit) {
        fun onRetry() {
            retry.invoke()
        }
    }
}