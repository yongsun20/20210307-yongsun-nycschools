package com.yongsun.nycschools.data.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.yongsun.nycschools.data.api.SchoolsApiService
import com.yongsun.nycschools.data.database.SchoolDatabase
import com.yongsun.nycschools.data.model.School
import com.yongsun.nycschools.data.model.Score
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

/**
 * Implementation of [SchoolRepository]
 */
class SchoolRepositoryImpl(
    private val schoolsApiService: SchoolsApiService,
    private val schoolDatabase: SchoolDatabase
) : SchoolRepository {
    @OptIn(ExperimentalPagingApi::class)
    override fun fetchSchools(): Flow<PagingData<School>> {
        return Pager(
            PagingConfig(
                pageSize = 10,
                enablePlaceholders = false,
                prefetchDistance = 1,
                initialLoadSize = 10
            ),
            remoteMediator = SchoolRemoteMediator(schoolsApiService, schoolDatabase),
            pagingSourceFactory = { schoolDatabase.schoolDao().getSavedSchools() }
        ).flow
    }

    override suspend fun fetchScore(dbn: String): Response<List<Score>> {
        return schoolsApiService.getScore(dbn)
    }
}