package com.yongsun.nycschools.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Wrapper of offset for Room Database
 */
@Entity(tableName = "schoolOffset")
data class SchoolOffset(
    @PrimaryKey val id: Int,
    val offset: Int
)