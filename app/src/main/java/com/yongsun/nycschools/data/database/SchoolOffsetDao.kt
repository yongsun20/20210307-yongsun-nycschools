package com.yongsun.nycschools.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yongsun.nycschools.data.model.SchoolOffset

/**
 * Dao for the offset number for School paging from NYC Open Data API
 */
@Dao
interface SchoolOffsetDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveOffset(offset: SchoolOffset)

    @Query("SELECT * FROM schoolOffset ORDER BY id DESC")
    suspend fun getOffsets(): List<SchoolOffset>
}