package com.yongsun.nycschools.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

/**
 * Data model for School
 *
 */
@Parcelize
@Entity(tableName = "schools")
data class School(
    @PrimaryKey val dbn: String,
    val school_name: String,
    val overview_paragraph: String,
    val phone_number: String,
    val website: String,
    val borough: String
) : Parcelable