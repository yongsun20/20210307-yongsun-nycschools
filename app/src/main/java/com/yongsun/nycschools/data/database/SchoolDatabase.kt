package com.yongsun.nycschools.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.yongsun.nycschools.data.model.SchoolOffset
import com.yongsun.nycschools.data.model.School

/**
 * Room Database for Schools
 */
@Database(entities = [School::class, SchoolOffset::class], version = 1)
abstract class SchoolDatabase : RoomDatabase() {

    abstract fun schoolDao(): SchoolDao
    abstract fun schoolOffsetDao(): SchoolOffsetDao

    companion object {

        fun create(context: Context): SchoolDatabase {

            return Room.databaseBuilder(
                context,
                SchoolDatabase::class.java,
                "schools"
            ).build()
        }
    }
}