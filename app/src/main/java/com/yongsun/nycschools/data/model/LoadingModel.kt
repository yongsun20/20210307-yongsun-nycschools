package com.yongsun.nycschools.data.model

/**
 * Data model for displaying the header and footer loading spinner
 *
 * @param progressBarVisibility
 * @param errorMessageVisibility
 * @param retryVisibility
 * @param errorMessage
 */
data class LoadingModel(
    val progressBarVisibility: Int,
    val errorMessageVisibility: Int,
    val retryVisibility: Int,
    val errorMessage: String
)