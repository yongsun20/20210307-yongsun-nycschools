package com.yongsun.nycschools.data.api

import com.yongsun.nycschools.data.model.School
import com.yongsun.nycschools.data.model.Score
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * API Service to fetch nyc schools from NYC Open Data
 */
interface SchoolsApiService {

    /**
     * Get a list of schools from NYC Open Data
     * @param limit the number of results to return
     * @param offset the paging index
     */
    @GET("s3k6-pzi2.json")
    suspend fun getSchools(
        @Query("\$limit") limit: String,
        @Query("\$offset") offset: String
    ): Response<List<School>>

    /**
     * Get average SAT scores for [School]
     * @param dbn school ID
     */
    @GET("f9bf-2cp4.json")
    suspend fun getScore(
        @Query("dbn") dbn: String
    ): Response<List<Score>>
}