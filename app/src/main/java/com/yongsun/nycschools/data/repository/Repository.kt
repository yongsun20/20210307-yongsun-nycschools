package com.yongsun.nycschools.data.repository

import androidx.paging.PagingData
import com.yongsun.nycschools.data.model.School
import com.yongsun.nycschools.data.model.Score
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

/**
 * Repository for fetching schools
 */
interface SchoolRepository {

    /**
     * Fetch a list of schools
     */
    fun fetchSchools(): Flow<PagingData<School>>

    /**
     * Fetch average SAT scores for a school
     * @param dbn school ID
     */
    suspend fun fetchScore(dbn: String): Response<List<Score>>
}