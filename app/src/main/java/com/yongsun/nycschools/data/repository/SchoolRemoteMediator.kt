package com.yongsun.nycschools.data.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.yongsun.nycschools.data.api.SchoolsApiService
import com.yongsun.nycschools.data.database.SchoolDatabase
import com.yongsun.nycschools.data.model.School
import com.yongsun.nycschools.data.model.SchoolOffset
import retrofit2.HttpException
import java.io.IOException

/**
 * Remote Mediator for fetching Schools. The mediator fetches local database first then fetch from
 * remote sources
 */
@OptIn(ExperimentalPagingApi::class)
class SchoolRemoteMediator(
    private val schoolsApiService: SchoolsApiService,
    private val schoolDatabase: SchoolDatabase
) : RemoteMediator<Int, School>() {

    companion object {
        private const val LIMIT = 10
    }

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, School>
    ): MediatorResult {
        return try {
            val savedOffset = when (loadType) {
                LoadType.REFRESH -> null
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    state.lastItemOrNull()
                        ?: return MediatorResult.Success(endOfPaginationReached = true)

                    schoolDatabase.schoolOffsetDao().getOffsets().firstOrNull()
                }
            }

            val offset = savedOffset?.offset?.toString() ?: "0"

            val response = schoolsApiService.getSchools(
                limit = LIMIT.toString(),
                offset = offset
            )

            if (response.isSuccessful) {
                val schools = response.body() ?: listOf()

                schoolDatabase.withTransaction {
                    if (loadType == LoadType.REFRESH) {
                        schoolDatabase.schoolDao().deleteSchools()
                    }

                    schoolDatabase.schoolOffsetDao().saveOffset(SchoolOffset(0, offset.toInt() + LIMIT))
                    schoolDatabase.schoolDao().saveSchools(schools)
                }
                MediatorResult.Success(endOfPaginationReached = true)
            } else {
                MediatorResult.Error(Exception(response.message()))
            }
        } catch (exception: IOException) {
            MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            MediatorResult.Error(exception)
        }
    }
}