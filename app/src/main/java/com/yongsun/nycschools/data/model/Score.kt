package com.yongsun.nycschools.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Data model for Score
 *
 */
@Parcelize
data class Score(
    val dbn: String,
    @SerializedName("school_name") val schoolName: String,
    @SerializedName("sat_critical_reading_avg_score") val criticalReadingScore: String,
    @SerializedName("sat_math_avg_score") val mathScore: String,
    @SerializedName("sat_writing_avg_score")val writingScore: String
) : Parcelable