package com.yongsun.nycschools.data.database

import androidx.paging.PagingSource
import androidx.room.*
import com.yongsun.nycschools.data.model.School

/**
 * Dao for list of Schools
 */
@Dao
interface SchoolDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveSchools(schools: List<School>)

    @Query("SELECT * FROM schools")
    fun getSavedSchools(): PagingSource<Int, School>

    @Query("DELETE FROM schools")
    suspend fun deleteSchools()
}