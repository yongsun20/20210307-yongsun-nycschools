package com.yongsun.nycschools.di

import com.yongsun.nycschools.data.database.SchoolDatabase
import com.yongsun.nycschools.data.repository.SchoolRepository
import com.yongsun.nycschools.data.repository.SchoolRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

fun appModule() = module {

    single { SchoolDatabase.create(androidContext()) } // database

    single { get<SchoolDatabase>().schoolDao() } // dao

    single { get<SchoolDatabase>().schoolOffsetDao() } // dao

    single { SchoolRepositoryImpl(get(), get()) as SchoolRepository} // repository
}