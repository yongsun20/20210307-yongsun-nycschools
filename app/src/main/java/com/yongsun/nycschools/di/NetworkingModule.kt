package com.yongsun.nycschools.di

import com.google.gson.GsonBuilder
import com.yongsun.nycschools.data.api.SchoolsApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://data.cityofnewyork.us/resource/"

fun networkingModule() = module {
    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            })
            .build()
    }

    single {
        GsonConverterFactory.create(
            GsonBuilder().create()
        )
    } // gson converter

    single {
        Retrofit.Builder()
            .addConverterFactory(get<GsonConverterFactory>())
            .client(get<OkHttpClient>())
            .baseUrl(BASE_URL)
            .build()
    }

    single { get<Retrofit>().create(SchoolsApiService::class.java) }
}