package com.yongsun.nycschools.di

import com.yongsun.nycschools.ui.scores.ScoreViewModel
import com.yongsun.nycschools.ui.main.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

fun viewModelModule() = module {
    viewModel { MainViewModel(get()) }
    viewModel { ScoreViewModel(get()) }
}